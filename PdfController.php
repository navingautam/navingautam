<?php
/*
 * Generates pdf of the invoice and sends it as email attachment
 * Formats pdf with data from invoice and other related tables 
 */
class PdfController extends Controller {
	public function filters() {
		return array (
				'accessControl' // perform access control for CRUD operations
		);
	}
	public function accessRules() {
		return array (
				array (
						'allow', // allow all users to perform 'index' and 'view' actions
						'actions' => array ('invoiceemail'),
						'users' => array ('@') 
				),
				array (
						'deny', // deny all users
						'users' => array ('*') 
				) 
		);
	}
	## Formats and send invoice in email
	public function actionInvoiceemail($id) {
		// Find invoice
		$invoiceRow = Invoice::model ()->findByPk ( $id );
		if ($invoiceRow === null)
			throw new CHttpException ( 404, 'The requested page does not exist.' );
		
		// Format and send email on email form submit
		if (isset ( $_POST ['sendemail'] )) {
			$emaildata = '';
			
			//Get email details from config table
			$config = Config::model ()->find ();
			
			if (isset ( $_POST ['autoId'] )) :
				foreach ( $_POST ['autoId'] as $addrId ) {
					$addrModel = Addressimport::model ()->findByPk ( $addrId );
					$emaildata .= $addrModel->email . ',';
				}
			
				endif;
			$mailTo = $_POST ['emailTo'] == '' ? Yii::app ()->user->email . "," . $emaildata : $emaildata . $_POST ['emailTo'];
			$subject = $_POST ['subject'];
			$message = $_POST ['message'];
			$showEmail = $_POST ['emailTo'];
			ActiveRecordLog::saveEmailInvoice ( $_GET ['id'] );
			
			// Temporaryfilename
			$fileName = 'Invoice_' . time ();
			
			// Get data for invoice
			$labArr = $this->invoiceLabList ( $invoiceRow->id, $invoiceRow->jobId );
			$matArr = $this->invoiceMatList ( $invoiceRow->id, $invoiceRow->jobId );
			$expArr = $this->invoiceExpList ( $invoiceRow->id, $invoiceRow->jobId );
			$feesArr = $this->invoiceFeesList ( $invoiceRow->id, $invoiceRow->jobId );
			$othArr = $this->invoiceOthList ( $invoiceRow->id, $invoiceRow->jobId );
			
			$subTot = $labArr ['tot'] + $matArr ['tot'] + $expArr ['tot'] + $feesArr ['tot'] + $othArr ['tot'];
			$totArr = $this->invoiceTotList ( $invoiceRow->id );
			if ($totArr ['data']) {
				$labArr ['data'] = '';
				$matArr ['data'] = '';
				$expArr ['data'] = '';
				$subTot = $totArr ['tot'] + $othArr ['tot'];
			}
			Yii::app ()->session ['InvsubTot' . $invoiceRow->id] = $subTot;
			
			// Generat pdf invoice
			$this->renderPartial ( 'mail/invoice', array (
					'id' => $_GET ['id'],
					'mailFileName' => $fileName,
					'labArr' => $labArr,
					'matArr' => $matArr,
					'expArr' => $expArr,
					'feesArr' => $feesArr,
					'othArr' => $othArr,
					'subTot' => $subTot 
			) );
			
			$filePath = Yii::app ()->getBasePath () . DIRECTORY_SEPARATOR . '../' . Config::baseDir () . '/' . $fileName . '.pdf';
			$content = chunk_split ( base64_encode ( file_get_contents ( $filePath ) ) );
			
			
			$uid = md5 ( uniqid ( time () ) );
			$name = basename ( $fileatt );
			
			$from_mail = $config->email ? $config->email : 'reports@ams.com.au';
			
			
			$userEmail = Users::model ()->findByPk ( Yii::app ()->user->id )->email;
			$from_mail = $userEmail;
			
			// Format mail message
			$header = "From: <" . $from_mail . ">\r\n";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
			$header .= "This is a multi-part message in MIME format.\r\n";
			$header .= "--" . $uid . "\r\n";
			$header .= "Content-type:text/html; charset=iso-8859-1\r\n";
			$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
			$header .= $message . "\r\n";
			$header .= "--" . $uid . "\r\n";
			$header .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n"; // use different content types here
			$header .= "Content-Transfer-Encoding: base64\r\n";
			$header .= "Content-Disposition: attachment; filename=\"Invoice_$id.pdf\"\r\n\r\n";
			$header .= $content . "\r\n\r\n";
			$header .= "--" . $uid . "--";
			
			if (mail ( $mailTo, $subject, $message, $header )) // Send mail and set flash message to success if mail sending is successful
				Yii::app ()->user->setFlash ( 'invoiceEmail', 'Email with Attachment sent successfully.' );
			else	// Set flash message to failure if mail sending fails
				Yii::app ()->user->setFlash ( 'invoiceEmailFail', 'Email Sending Failed.' );
			
			// Remove temporary invoice pdf file
			if (file_exists ( $filePath ))
				unlink ( $filePath );
			
			//Redirect to invoiveemail page
			$this->redirect ( array
					(
						'invoiceemail',
						'id' => $id 
					)
			 );
		}
		//Render invoiceemail page
		$this->render ( 'invoiceemail', array (
				'invoiceRow' => $invoiceRow 
		) );
	}
	
	// Get Labour details for generating invoice pdf
	public function invoiceLabList($id, $jobId) {
		$invModel = Invoice::model ()->findByPk ( $id );
		$crt = new CDbCriteria ();
		$crt->condition = 'invoiceId=' . $id . ' AND claimStatus=' . Invoiceclaims::LAB;
		$crt->order = 'id desc';
		$row = Invoiceclaims::model ()->find ( $crt );
		$data = '';
		$cnt = 0;
		if ($row && $row->useSubtotal || ($row && ! $row->useSubtotal && ! $row->breakDown && ! $row->labBreakDetail)) {
			$data .= '<tr>
						<td style_="border-left:1px solid black"><b>Labour</b></td>
						 <td style_="border-left:1px solid black"></td>
						 <td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">
						 	' . Common::money_format ( '%n', $row->price ) . '</td>
					</tr>';
			$cnt ++;
		} elseif ($row->breakDown) {
			
			$model = Invoiceclaimhour::model ()->findAllByAttributes ( array (
					'invoiceClaimId' => $row->id 
			) );
			if ($model) {
				$data .= '<tr>
						<td style_="border-left:1px solid black"><b>Labour</b></td>
						 <td style_="border-left:1px solid black"></td>
						 <td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">
						 	</td>
					</tr>';
				$cnt ++;
			}
			foreach ( $model as $key => $val ) {
				$workTypeRw = Worktype::model ()->findByPk ( $val->workTypeId );
				$wrkType = $workTypeRw ? $workTypeRw->name : '';
				
				if ($val->ordHr) {
					$data .= '<tr>
						<td style_="border-left:1px solid black">' . $wrkType . '</td>
						 <td style_="border-left:1px solid black" align="right">' . $val->ordHr . '</td>
						 <td style_="border-left:1px solid black" align="right">' . Common::money_format ( '%n', $val->ordPrice ) . '</td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">
						 	' . Common::money_format ( '%n', $val->ordHr * $val->ordPrice ) . '</td>
					</tr>';
					$cnt ++;
				} elseif ($val->otHr) {
					$data .= '<tr>
						<td style_="border-left:1px solid black">' . $wrkType . '</td>
						 <td style_="border-left:1px solid black" align="right">[OT] ' . $val->otHr . '</td>
						 <td style_="border-left:1px solid black" align="right">' . Common::money_format ( '%n', $val->otPrice ) . '</td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">
						 	' . Common::money_format ( '%n', $val->otHr * $val->otPrice ) . '</td>
					</tr>';
					$cnt ++;
				}
			}
		} elseif ($row->labBreakDetail) {
			$cnt ++;
			
			$crt = new CDbCriteria ();
			$crt->condition = 'invoiceId=' . $invModel->id;
			$crt->with = array (
					'jobsLabourInf' => array (
							'alias' => 'j',
							'joinType' => 'INNER JOIN' 
					) 
			);
			$labRws = Invoiceclaimdetaillab::model ()->findAll ( $crt );
			
			$labDetsell = 0;
			if ($labRws) {
				$data .= '<tr>
						<td style_="border-left:1px solid black"><b>Labour</b></td>
						 <td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">
						 	</td>
					</tr>';
				$cnt ++;
			}
			foreach ( $labRws as $key => $val ) {
				$usrRw = Users::model ()->findByPk ( $val->jobsLabourInf->techId );
				$workTypeRw = Worktype::model ()->findByPk ( $val->jobsLabourInf->workTypeId );
				if ($val->ordHr) {
					$data .= '<tr>
							<td style_="border-left:1px solid black">
								<table border="0">
								<tr><td align="left">' . $usrRw->name . ' </td>
								<td align="left">' . $workTypeRw->name . '</td>
								<td align="right">' . date ( 'd-m-Y', strtotime ( $val->jobsLabourInf->date ) ) . '</td></tr></table>
							</td>
				
							 <td style_="border-left:1px solid black" align="right">' . $val->ordHr . '</td>
							 <td style_="border-left:1px solid black" align="right">' . Common::money_format ( '%n', $val->ordPrice ) . '</td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">
						 	' . Common::money_format ( '%n', $val->ordHr * $val->ordPrice ) . '</td>
					</tr>';
					$cnt ++;
				}
				if ($val->otHr) {
					$data .= '<tr>
							<td style_="border-left:1px solid black">
								<table border="0">
								<tr><td align="left">' . $usrRw->name . ' </td>
								<td align="left">' . $workTypeRw->name . '</td>
								<td align="right">' . date ( 'd-m-Y', strtotime ( $val->jobsLabourInf->date ) ) . '</td></tr></table>
							</td>
				
							 <td style_="border-left:1px solid black" align="right">[OT] ' . $val->otHr . '</td>
							 <td style_="border-left:1px solid black" align="right">' . Common::money_format ( '%n', $val->otPrice ) . '</td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">
						 	' . Common::money_format ( '%n', $val->otHr * $val->otPrice ) . '</td>
					</tr>';
					$cnt ++;
					//
				}
			}
		}
		
		$dataArr = array ();
		$dataArr ['data'] = $data;
		$dataArr ['tot'] = $row->price;
		$dataArr ['cnt'] = $cnt;
		return $dataArr;
	}
	
	// Get Material details for generating invoice pdf
	public function invoiceMatList($id, $jobId) {
		$invModel = Invoice::model ()->findByPk ( $id );
		$crt = new CDbCriteria ();
		$crt->condition = 'invoiceId=' . $id . ' AND claimStatus=' . Invoiceclaims::MAT;
		$crt->order = 'id desc';
		$model = Invoiceclaims::model ()->find ( $crt );
		$data = '';
		$cnt = 0;
		if ($model->breakDown) {
			$invModel = Invoice::model ()->findByPk ( $id );
			
			$crt = new CDbCriteria ();
			$crt->condition = 'invoiceId=' . $invModel->id;
			$crt->with = array (
					'materialInf' => array (
							'alias' => 'm',
							'joinType' => 'INNER JOIN' 
					) 
			);
			$row = Invoiceclaimdetailmat::model ()->findAll ( $crt );
			
			## Format data to send to pdf generator
			$data .= '<tr>
							<td style_="border-left:1px solid black"><b>Materials</b></td>
							<td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right"></td>
					</tr>';
			$cnt ++;
			foreach ( $row as $val ) {
				$data .= '<tr>
						<td style_="border-left:1px solid black">' . ucwords ( $val->materialInf->item ) . '</td>
							<td style_="border-left:1px solid black" align="right">' . $val->qnty . '</td>
							<td style_="border-left:1px solid black" align="right">' . Common::money_format ( '%n', $val->rate ) . '</td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">
							 ' . Common::money_format ( '%n', $val->qnty * $val->rate ) . '</td>
					</tr>';
				$cnt ++;
			}
		} elseif ($model && ! $model->breakDown) {
			$data .= '<tr>
			<td style_="border-left:1px solid black"><b>Materials</b></td>
			<td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">' . Common::money_format ( '%n', $model->price ) . '</td>
					</tr>';
			$cnt ++;
		}
		
		$dataArr = array ();
		$dataArr ['data'] = $data;
		$dataArr ['tot'] = $model->price;
		$dataArr ['cnt'] = $cnt;
		return $dataArr;
	}
	
	// Get Expense details for generating invoice pdf
	public function invoiceExpList($id, $jobId) {
		$invModel = Invoice::model ()->findByPk ( $id );
		$crt = new CDbCriteria ();
		$crt->condition = 'invoiceId=' . $id . ' AND claimStatus=' . Invoiceclaims::EXP;
		$crt->order = 'id desc';
		$model = Invoiceclaims::model ()->find ( $crt );
		$data = '';
		$cnt = 0;
		if ($model->breakDown) {
			$invModel = Invoice::model ()->findByPk ( $id );
			
			$crt = new CDbCriteria ();
			$crt->condition = 'invoiceId=' . $invModel->id;
			$crt->with = array (
					'expenseInf' => array (
							'alias' => 'e',
							'joinType' => 'INNER JOIN' 
					) 
			);
			$row = Invoiceclaimdetailexp::model ()->findAll ( $crt );
			
			## Format data to send to pdf generator
			$data .= '<tr>
			<td style_="border-left:1px solid black"><b>Expenses</b></td>
					<td style_="border-left:1px solid black" align="right"></td>
			<td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right"></td>
					</tr>';
			$cnt ++;
			foreach ( $row as $val ) {
				$data .= '<tr>
						<td style_="border-left:1px solid black">' . ucwords ( $val->expenseInf->item ) . '</td>
						 <td style_="border-left:1px solid black" align="right">' . $val->qnty . '</td>
						 <td style_="border-left:1px solid black" align="right">' . Common::money_format ( '%n', $val->rate ) . '</td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">
						 	' . Common::money_format ( '%n', $val->qnty * $val->rate ) . '</td>
					</tr>';
				$cnt ++;
			}
		} elseif ($model && ! $model->breakDown) {
			$data .= '<tr>
						<td style_="border-left:1px solid black"><b>Expenses</b></td>
							<td style_="border-left:1px solid black" align="right"></td>
							<td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">' . Common::money_format ( '%n', $model->price ) . '</td>
					</tr>';
			$cnt ++;
		}
		$dataArr = array ();
		$dataArr ['data'] = $data;
		$dataArr ['tot'] = $model->price;
		$dataArr ['cnt'] = $cnt;
		return $dataArr;
	}
	
	// Get Fee details for generating invoice pdf
	public function invoiceFeesList($id, $jobId) {
		$invModel = Invoice::model ()->findByPk ( $id );
		$crt = new CDbCriteria ();
		$crt->condition = 'invoiceId=' . $id . ' AND claimStatus=' . Invoiceclaims::FEES;
		$crt->order = 'id desc';
		$model = Invoiceclaims::model ()->find ( $crt );
		$data = '';
		$cnt = 0;
		if ($model->breakDown) {
			$invModel = Invoice::model ()->findByPk ( $id );
			
			$crt = new CDbCriteria ();
			$crt->condition = 'invoiceId=' . $invModel->id;
			$crt->with = array (
					'jobsFeesInf' => array (
							'alias' => 'jf',
							'joinType' => 'INNER JOIN' 
					) 
			);
			$row = Invoiceclaimdetailfees::model ()->findAll ( $crt );
			
			## Format data to send to pdf generator
			$data .= '<tr>
			<td style_="border-left:1px solid black"><b>Service Fees</b></td>
			<td style_="border-left:1px solid black" align="right"></td>
			<td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right"></td>
					</tr>';
			$cnt ++;
			foreach ( $row as $val ) {
				$feesRw = Fees::model ()->findByPk ( $val->jobsFeesInf->feesId );
				
				$data .= '<tr>
						<td style_="border-left:1px solid black">' . ucwords ( $feesRw->name ) . '</td>
							<td style_="border-left:1px solid black" align="right">' . $val->qnty . '</td>
							<td style_="border-left:1px solid black" align="right">' . Common::money_format ( '%n', $val->sellRate ) . '</td>
							<td style_="border-left:1px solid black;border-right:1px solid black" align="right">
						 	' . Common::money_format ( '%n', $val->qnty * $val->sellRate ) . '</td>
							 	</tr>';
				$cnt ++;
			}
		} elseif ($model && ! $model->breakDown) {
			$data .= '<tr>
						<td style_="border-left:1px solid black"><b>Service Fees</b></td>
							<td style_="border-left:1px solid black" align="right"></td>
			<td style_="border-left:1px solid black" align="right"></td>
						 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">' . Common::money_format ( '%n', $model->price ) . '</td>
					</tr>';
			$cnt ++;
		}
		$dataArr = array ();
		$dataArr ['data'] = $data;
		$dataArr ['tot'] = $model->price;
		$dataArr ['cnt'] = $cnt;
		return $dataArr;
	}
	
	// Get other details for generating invoice pdf
	public function invoiceOthList($id, $jobId) {
		$invModel = Invoice::model ()->findByPk ( $id );
		$crt = new CDbCriteria ();
		$crt->condition = 'invoiceId=' . $id . ' AND claimStatus=' . Invoiceclaims::CUSTOM;
		$crt->order = 'claims';
		$models = Invoiceclaims::model ()->findAll ( $crt );
		$data = '';
		$tot = 0;
		$cnt = 0;
		
		// If calims exist format, data to send to pdf generator
		if ($models) {
			$data .= '<tr>
				<td style_="border-left:1px solid black"><b>Other Items</b></td>
				<td style_="border-left:1px solid black" align="right"></td>
				<td style_="border-left:1px solid black" align="right"></td>
				<td style_="border-left:1px solid black;border-right:1px solid black" align="right"></td>
						</tr>';
			$cnt ++;
		}
		foreach ( $models as $key => $val ) {
			
			$data .= '<tr>
							<td style_="border-left:1px solid black">' . ucwords ( $val->claims ) . '</td>
							 <td style_="border-left:1px solid black" align="right"></td>
							 <td style_="border-left:1px solid black" align="right"></td>
								 <td style_="border-left:1px solid black;border-right:1px solid black" align="right">' . Common::money_format ( '%n', $val->price ) . '</td>
						</tr>';
			$cnt ++;
			$tot += $val->price;
		}
		$dataArr = array ();
		$dataArr ['data'] = $data;
		$dataArr ['tot'] = $tot;
		$dataArr ['cnt'] = $cnt;
		return $dataArr;
	}
}